from flask import Flask
from flask_mysqldb import MySQL

app = Flask(__name__)

# Configurer la connexion à la base de données MySQL
# app.config['MYSQL_HOST'] = 'db'
# app.config['MYSQL_USER'] = 'root'
# app.config['MYSQL_PASSWORD'] = 'password'
# app.config['MYSQL_DB'] = 'mydb'
# mysql = MySQL(app)

@app.route('/')
def index():
    return 'Hello World from the Flask Server!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
