CREATE DATABASE IF NOT EXISTS mydatabase;
USE mydatabase;

CREATE TABLE IF NOT EXISTS login (
  email VARCHAR(256) PRIMARY KEY,
  password VARCHAR(256),
  created_at DATE DEFAULT NOW()
);

INSERT INTO login (email, password) VALUES ('admin@test.com', 'admin');
