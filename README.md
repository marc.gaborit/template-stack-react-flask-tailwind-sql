# Template for deploying easily a React-Flask-MySQL app

## Display

![3 displays](doc/images/3-display.png)

## Main Idea

> While **Docker is used to create micro-services**, **Docker-compose** is a DevOps layer above which is **in charge of the communication between services**. 

> After the **contenerisation**, we discover another solution for another paradigm : docker-compose for the paradigm of **orchestration** as opposed to docker for the paradigm of contenerisation. Such DevOps solutions are : Docker-compose, Docker Swarm, Kubernetes...

## Requirements

* docker and docker-compose (installed by default with docker)

> **That's it if you dev in the containers** (no python3 with a venv or nvm, node18 and npm)

**Otherwise, on your host install :**

* **Front :** nvm, nodejs18, npm, create a Vite JS Project and add Tailwind.css configuration
* **Back :** python3.8, venv, poetry
* **MariaDB and PhpMyAdmin :** won't give you the manual install, please go on docker for the SQL instance it's much easier I assure you

## Project tree

We clearly see the separation of our project in term of micro-services. Each will now be containerise

![Project Tree](doc/images/project-tree.png)

## Deployment of the entire App

```bash
docker-compose up --build # ensure that the ports 3306 (MariaDB), 8080 (PhpMyAdmin), 5000 (Flask) and 5173 (React) are available
```

### Lien vers microservices

* **Front React :**
    * <http://10.1.0.20:5173>
* **Back Flask :**
    * <http://10.1.0.21:5000>
* **PhpMyAdmin :**
    * <http://10.1.0.22/>


## Details

### Infrastructure choices

#### Network

* For code factorisation and multi-nodes purposes we won't use localhost to call our micro-services (good habits for prod)
* To let know that for developper experience I put static IPs. Check that it doesn't overwhelm on other of your networks.

#### Dev in container

With the *volume* parameter written in each of our micro-services in our docker-compose we can now save our work and observe its impact on a web page. 

Only exceptions seen that need a npm restart :

* new package npm
* ...

#### Advantages and Drawbacks

> **I know that my dev install is heavy, however I find it more convenient**. indeed there are only 2 terminals to watch now :
> 
> * the docker-compose interactive terminal
> * the web JS console
> 

#### Out of the scope of docker-compose but good to know

* In Kubernetes we can dynamically replicate micro-services, therefore we won't think with static IPs and let the container engine do it for us

## Micro-services

### Front React

> After docker-compose run, Access : <http://10.1.0.20:5173/> 

#### How to create it from scratch

* Create a Vite JS project and add Tailwind.css : <https://tailwindcss.com/docs/guides/vite>
* test it : 

```bash
npm run dev
# React will say that you need to configure the --host in your package.json, do so for running your container 
```

* Add a Dockerfile in your project (see mine)
* Check the service front-react of the docker-compose.yaml (see mine) file which is in charge of building and running the app. You will understand how it is deploy and which syntax to use, careful for indentation on yaml file btw.

### Back Python

> After docker-compose run, Access : <http://10.1.0.21:5000>

#### Remarques

* It's not a big project so for a lightweight image we won't use poetry
* I assume that Docker is already an environment so no venv in our docker image, we will use a requirements.txt file

### MariaDb && PhpMyAdmin

> After docker-compose run, Access : <http://10.1.0.22/> : <root:root>

#### Manage Data

* The file **mysql/db-init/init.sql** allows you to initialise your DB MariaDB at the docker runtime
* Once you've done the docker-compose run, modify and check your data on the phpmyadmin portal web : <http://10.1.0.22/> :

![Phpmyadmin web portal](doc/images/phpmyadmin-dashboard.png)